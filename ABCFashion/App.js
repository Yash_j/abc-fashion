import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/components/Home';
import Login from './src/components/Login';
import Profile from './src/components/Profile';
import Cart from './src/components/Cart'; 
import ABCFashion from './src/components/ABCFashion';
import Search from './src/components/Search';
import More from './src/components/More';
import OrderPlacedScreen from './src/components/OrderPlaced';
import ProductDetail from './src/components/ProductDetail';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Ultra-Fashion" component={ABCFashion} />
        <Stack.Screen name="OrderPlaced" component={OrderPlacedScreen}/>
        <Stack.Screen name="ProductDetail" component={ProductDetail}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;