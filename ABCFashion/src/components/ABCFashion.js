import React from 'react';  
import {StyleSheet, Text, View} from 'react-native';  
import { createAppContainer } from 'react-navigation';
// import { createBottomTabNavigator } from 'react-navigation-tabs'; 
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';    
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import Profile from './Profile';
import Home from './Home';
import Cart from './Cart';
import Search from './Search';
import More from './More';
const Tab = createBottomTabNavigator();



const Tabcontainer = createAppContainer(Tab);

const ABCFashion = ({navigation}) =>{
        return (
          <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;
  
              if (route.name === 'Home') {
                iconName = 'ios-home-outline';
              } else if (route.name === 'Search') {
                iconName = 'ios-search-outline';
              } else if (route.name === 'Cart') {
                iconName = 'ios-cart-outline';
              } else if (route.name === 'Profile') {
                // iconName = 'ios-cart-outline';
                return <IconSimpleLine name="user" color={color} size={size}/>
              }else if (route.name === 'More') {
                iconName = 'ios-list';
              }
  
              // You can return any component that you like here!
              return <Icon name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: '#ff6969',
            inactiveTintColor: '#696969',
          }}
  
          >
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="Search" component={Search} />
            <Tab.Screen name="Cart" component={Cart} />
            <Tab.Screen name="Profile" component={Profile} />
            <Tab.Screen name="More" component={More} />

          </Tab.Navigator>
        );
}

export default ABCFashion; 