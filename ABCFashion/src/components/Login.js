import React, { useEffect, useState } from 'react';
import { View, Dimensions, StyleSheet, TextInput, Text, Image, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import IconFeather from 'react-native-vector-icons/Feather';



const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: "flex-start",
    },
    containerLandscape: {
        // backgroundColor: '#000',
        // justifyContent: "center", alignItems: "center"
    },
    box: {
        backgroundColor: 'white',
        height: 200,
    },

    loginInput: {
        height: 150,
        backgroundColor: "#ffffff",
        borderRadius: 25,
        justifyContent: "space-around",
    },

    logbutton: {
        // width: "80%",
        backgroundColor: "#ff6969",
        borderRadius: 25,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10,
        marginLeft: 20
    },

    loginText: {
        color: "white",
        fontWeight: "bold",
        justifyContent: "center",
        alignItems: "center",
        marginLeft: "20%",
        marginTop: 5,
        marginBottom: 5
    },

    arrowlogo: {
        color: "white",
        left: 50,
        marginLeft: "20%"
    },

    textinput: {
        marginLeft: 15,
        marginTop: 15,
        // fontFamily: "sans-serif-medium"
    },

});

// const screen = Dimensions.get('screen');

const useScreenDimensions = () => {
    const [screenData, setScreenData] = useState(Dimensions.get('screen'));

    useEffect(() => {
        const onChange = (result) => {
            setScreenData(result.screen);
            // console.log(">>>>>>",result)
        };

        Dimensions.addEventListener('change', onChange);

        return () => Dimensions.removeEventListener('change', onChange);
    });

    return {
        ...screenData,
        isLandscape: screenData.width > screenData.height,
        numsColumn: (screenData.width > screenData.height) ? 3: 2
    };
};

export default Login = ({ navigation }) => {
    const screenData = useScreenDimensions();

    // console.log(screenData);
    return (
        <View
            style={[
                styles.container,
                screenData.isLandscape && styles.containerLandscape,
            ]}
        >
            <ScrollView
            showsVerticalScrollIndicator = {false}
            >
                <View style={{justifyContent:"center",alignItems: "center"}}>
                <Image source={require('../assets/Ultra-Fashion.png')} />
                </View>

                <View style={[styles.loginInput, { width: screenData.width / 1.2 , marginLeft: 15}]}>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                        <IconSimpleLine name="user" size={20} style={{ marginTop: 20, marginLeft: 10, color: "#696969" }} />
                        <TextInput style={styles.textinput} placeholder="USERNAME / EMAIL"/>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: "center" }}>
                        <IconFeather name="lock" size={20} style={{ marginTop: 20, marginLeft: 10, color: "#696969" }} />
                        <TextInput style={styles.textinput} secureTextEntry={true} placeholder="PASSWORD" />
                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => navigation.navigate('Ultra-Fashion')}
                    // style={styles.logbutton}
                    style={[styles.logbutton, { width: screenData.width / 1.2 }]}
                >
                    <View style={{ flexDirection: 'row' , justifyContent: "center", alignItems: "center"}}>
                        <View style={{justifyContent: "flex-end", alignItems: "flex-end"}}>
                        <Text style={styles.loginText}>LOG IN </Text>
                        </View>
                        <View style={{justifyContent: "flex-end", alignItems: "flex-end"}}>
                        <IconAntDesign style={styles.arrowlogo} name="rightcircle" size={23} />
                        </View>
                    </View>
                </TouchableOpacity>

                <View style={{ justifyContent: "center", alignItems: "center" , marginTop: "10%"}}>
                    <Text>Don’t have an account? Swipe right to</Text>
                    <TouchableOpacity><Text style={{ color: "rgb(255,105,105)" }}>create a new account</Text></TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
};