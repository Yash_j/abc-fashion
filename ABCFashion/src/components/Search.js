import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Dimensions,
  FlatList,
  TextInput,
  Image,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ActivityIndicator,
  useWindowDimensions
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import IconFeather from 'react-native-vector-icons/Feather';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';
import { fetchData } from '../service/api';

// const isPortrait = () => {
//   const dim = Dimensions.get('screen');
//   return dim.height >= dim.width;
// };

// const isLandscape = () => {
//   const dim = Dimensions.get('screen');
//   return dim.width >= dim.height;
// };

// Dimensions.addEventListener('change', ()=>{
//   var width= isPortrait() ? screenwidth * 0.3 : screenwidth;
// })


const numColumns = 2;
// const screenwidth = Dimensions.get('window').width;
// const size = Dimensions.get('window').width / numColumns;


function SearchScreen({ navigation }) {

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [orientation, setOrientation] = useState("")
  const window = useWindowDimensions()

  const getOrientation = () => {
    if (window.height < window.width) {
        setOrientation("LANDSCAPE")
    } else {
        setOrientation("PORTRAIT")
    }
    return orientation
}

  useEffect(() => {
    fetchData().then((result) => {
      // console.log("result",result)
      setData(result)
      setLoading(false)
      getOrientation()
    })
  }, [])

  // console.log()
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row", backgroundColor: "white", width: "100%", justifyContent: "space-around" }}>
        <View>
          <IconEntypo name="chevron-left" size={27} style={{ color: "#ff6969", marginTop: "27%" }} />
        </View>
        <View style={styles.SectionStyle}>
          <Image source={require('../assets/SearchLogo.png')} style={styles.ImageStyle} />
          <TextInput
            style={styles.inputText}
            placeholder="Search"
            placeholderTextColor="#696969"
          />
        </View>
        <View>
          <IconAntDesign name="filter" size={25} style={{ marginTop: "25%", marginLeft: "5%", color: "#ff6969" }} />
        </View>
      </View>
      <View style={{ flexDirection: "row", height: 30, width: "100%", backgroundColor: "white" }}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}

        >
          <TouchableOpacity>
            <Text style={{ padding: 5 }}>BEST MATCH</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ padding: 5 }}>TOP RATED</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ padding: 5 }}>PRICE LOW TO HIGH</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ padding: 5 }}>PRICE HIGH TO LOW</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
      {isLoading ? <ActivityIndicator /> : ((window.width>window.height)? 
      
      <FlatList
          horizontal={false}
          key={'#'}
          showsVerticalScrollIndicator = {false}
          data={data}
          renderItem={({ item }) => (
            <TouchableOpacity
            onPress={() => navigation.navigate('ProductDetail',{id:item.id})}
            >
            <View style={styles.landitemContainer}>
              <Image style={styles.landitem} source={{ uri: item.image }} />
              <Text style={{ justifyContent: "center", alignItems: "center" }}>{item.description}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.itemprice}>{item.Price}</Text>
                <View style={styles.itemstar}>
                <Text style={styles.itemstartext}>{item.rating}</Text>
                </View>
              </View>
            </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => "#"+item.id}
          numColumns={3}
          />  

      : 
        <FlatList
          horizontal={false}
          key={'_'}
          showsVerticalScrollIndicator = {false}
          data={data}
          renderItem={({ item }) => (
            <TouchableOpacity
            onPress={() => navigation.navigate('ProductDetail',{id:item.id})}
            >
            <View style={styles.itemContainer}>
              <Image style={styles.item} source={{ uri: item.image }} />
              <Text style={{ justifyContent: "center", alignItems: "center" }}>{item.description}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.itemprice}>{item.Price}</Text>
                <View style={styles.itemstar}>
                <Text style={styles.itemstartext}>{item.rating}</Text>
                </View>
              </View>
            </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => "_"+item.id}
          numColumns={2} 
          />
          
          )
          
          }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: "#333"
  },
  itemContainer: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 190,
    // borderRadius: 10,
    backgroundColor: "#fff",
    margin: 15,
    marginTop: "2%",
    borderRadius: 15
  },
  item: {
    position: 'relative',
    width: 100,
    height: 120,
  },
  inputText: {
    // height: 50,
    color: "#454545",
    fontSize: 18,
  },
  SectionStyle: {
    flexDirection: 'row',
    // borderWidth: 1,
    borderColor: '#A9A9A9',
    height: 40,
    borderRadius: 20,
    margin: 5,
    width: "75%",
    backgroundColor: "#F2F2F2"
    // marginLeft: "5%"
  },
  ImageStyle: {
    padding: 10,
    margin: 10,
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    alignItems: 'center'
  },
  itemstar: {
    height: 13,
    width: "15%",
    backgroundColor: "#ff6969",
    marginTop: "3%",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  itemprice: {
    marginRight: "10%",
    marginTop: "3%",
    fontWeight: "bold"
  },
  itemstartext: {
    color: "white",
    fontWeight: "900",
    fontSize: 10,

  },

  landitemContainer: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 170,
    height: 210,
    // borderRadius: 10,
    backgroundColor: "#fff",
    margin: 30,
    marginTop: "2%",
    borderRadius: 15
  },
  landitem: {
    position: 'relative',
    width: 140,
    height: 140,
  }
});

export default SearchScreen;
