import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  FlatList,
  Image,
  useWindowDimensions
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontisto from 'react-native-vector-icons/Fontisto';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';


const user = [
  { id: 1, name: "Jane Doe", Email: "janedoe123@email.com" }
]

function Profile({ navigation }) {
  const [notifyCount, setNotification] = useState(1);

  const window = useWindowDimensions();
  console.log(">>>>window.height",window.height)
  console.log(">>>>window.width",window.width)
  console.log(window.height > window.width ? "Portrait" : "Landscape")

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={{ bottom: 8 }}>
          <View style={{ flexDirection: "row", marginLeft: 280, marginTop: "2.5%" }}>
            <View style={styles.chatbadge}>
              <Text style={styles.badgetext}>{notifyCount}</Text>
            </View>
            <IconIonicons onPress={()=> alert('Message')} name="md-chatbubble-outline" size={20} style={{ color: "#696969" }} />
            <View style={styles.badge}>
              <Text style={styles.badgetext}>{notifyCount}</Text>
            </View>
            <IconFontAwesome onPress={()=> alert('Notification')} style={{ marginLeft: 20, color: "#696969" }} name="bell-o" size={20} />
          </View>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text style={styles.text}>{user[0].name}</Text>
            <Text style={{ fontSize: 15 }}>{user[0].Email}</Text>
          </View>
          <TouchableOpacity
            style={styles.editbutton}
            onPress={() => alert("Edit Button")}
          ><Text style={styles.editText}>EDIT PROFILE</Text></TouchableOpacity>
        </View>
        <View style={styles.menucontainer}>
          <TouchableOpacity><View style={styles.menufield}>
            {/* <Text>Hello</Text> */}
            <IconIonicons name="list" size={23} style={{ color: "#696969", right: 15 }} />
            <Text style={{ fontSize: 17, color: "#696969", right: 70 }}>All My Orders</Text>
            <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
          </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconMaterialIcons name="local-shipping" size={23} style={{ color: "#696969", right: 10 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 49 }}>Pending Shipments</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
            </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconMaterialIcons name="payment" size={23} style={{ color: "#696969", right: 13 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 50 }}>Pending Payments</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
            </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconEntypo name="shopping-bag" size={23} style={{ color: "#696969", right: 15 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 60 }}>Finished Orders</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
            </View></TouchableOpacity>

        </View>

        <View style={styles.menucontainer}>
          <TouchableOpacity><View style={styles.menufield}>
            {/* <Text>Hello</Text> */}
            <IconIonicons name="ios-mail-open-outline" size={23} style={{ color: "#696969", right: 15 }} />
            <Text style={styles.menufieldtext}>Invite Friends</Text>
            <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end", justifyContent: "flex-end" }} size={20} />
          </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconFontAwesome5 name="headset" size={23} style={{ color: "#696969", right: 13 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 50 }}>Customer Support</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" , justifyContent: "flex-end" }} size={20} />
            </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconFontAwesome name="star-o" size={23} style={{ color: "#696969", right: 20 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 70 }}>Rate Our App</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
            </View></TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.menufield}>
              {/* <Text>Hello</Text> */}
              <IconSimpleLineIcons name="note" size={23} style={{ color: "#696969", right: 15 }} />
              <Text style={{ fontSize: 17, color: "#696969", right: 50 }}>Make a Suggestion</Text>
              <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", alignItems: "flex-end" }} size={20} />
            </View></TouchableOpacity>

        </View>
        {/* <View style={{ marginTop: 25, flexDirection: "column"}}> */}
        {/* <FlatList
          data={ProfileItems}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={styles.listItem} >
              <TouchableOpacity onPress={()=>alert("Alert")}>
              <Image style={{ height: 25, width: 25 }} source={item.img} /></TouchableOpacity>
              <TouchableOpacity onPress={()=>alert("Alert")}><Text style={{ marginLeft: 20, fontSize: 17 }}>{item.title}</Text></TouchableOpacity>
              <TouchableOpacity onPress={()=>alert("Alert")}><IconEntypo name="chevron-with-circle-right" style={{marginLeft: item.marginvalue}} size={23}/></TouchableOpacity>
              
            </View>
          )}
        /> */}
        {/* </View> */}

      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  text: {
    fontWeight: "900",
    fontSize: 30,
    color: "#696969",
    // marginRight: 200,
    // marginBottom: 20
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
    marginBottom: 70
  },
  listItem: {
    width: 330,
    backgroundColor: "white",
    padding: 15,
    borderRadius: 10,
    borderBottomWidth: 0.3,
    flexDirection: "row",
    // marginBottom: "10%"
  },
  badge: {
    position: 'absolute',
    right: -10,
    top: 10,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },

  badgetext: {
    color: 'white',
    fontSize: 10,
    fontWeight: '900',
  },
  editbutton: {
    width: "50%",
    // backgroundColor: "#ff6969",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#A9A9A9",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: "25%",
    marginTop: "4%"
    // marginTop: 40,
    // marginBottom: 10
  },
  editText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "#808080"
  },

  menucontainer: {
    height: 200,
    width: "88%",
    borderRadius: 13,
    backgroundColor: "white",
    justifyContent: "space-around",
    marginTop: "5%"
  },

  menufield: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  menufieldtext: {
    fontSize: 17,
    color: "#696969",
    right: 70
  },

  chatbadge: {
    position: 'absolute',
    right: 30,
    top: 9,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Profile;