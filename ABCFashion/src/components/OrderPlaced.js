import React from 'react';
// import { View } from 'react-native';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Dimensions,
    FlatList,
    TextInput,
    Image,
    TouchableHighlight,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign'


function OrderPlacedScreen( { navigation } ) {
    return (
        <View style={styles.container}>
            <ScrollView
                showsVerticalScrollIndicator = {false}
                style={{marginTop: "10%"}}
            
            >
            {/* <Text>OrderPlaced</Text> */}
            <View style={{justifyContent: "center", alignItems: "center"}}>
            <View style={styles.iconcircle}>
                <IconMaterialIcons name="check" size={50} style={styles.checkicon} />
            </View>
            </View>
            <View style={{justifyContent: "center",alignItems: "center"}}>
            <Text style={styles.ordertext}>Order Placed!</Text>
            </View>
            <View style={{ marginTop: "10%", alignItems: "center" }}>
                <Text style={styles.textcolor}>Your order was placed successfully.</Text>
                <Text style={styles.textcolor}>For more details, check All My Orders</Text>
                <Text style={styles.textcolor}>page under Profile tab</Text>
                <View style={{marginTop: "20%"}}>
                <TouchableOpacity
                    style={styles.checkbutton}
                    onPress={() => navigation.navigate('Search')}
                >
                    <View style={{ flexDirection: "row" }}>
                        <Text style={styles.checkouttext}>ORDER MORE</Text>
                        <IconAntDesign style={{ color: "white", left: 23 }} name="rightcircle" size={32} />
                    </View>
                </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    checkicon: {
        color: "#ff6969",
    },
    iconcircle: {
        height: 120,
        width: 120,
        borderRadius: 60,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center"
    },
    ordertext: {
        fontSize: 30,
        color: "#56546B",
        marginTop: "5%",
        fontWeight: "normal"
    },
    textcolor: {
        color: "#56546B"
    },
    checkbutton: {
        width: 190,
        backgroundColor: "#ff6969",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        //  marginTop: 40,
        //    marginBottom: 10
      },
    checkouttext: {
        color: "white",
        fontWeight: "900",
        marginTop: "5%",
        // marginLeft: "5%"
      }
})

export default OrderPlacedScreen;