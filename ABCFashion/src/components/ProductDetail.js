import React, { useEffect, useState } from 'react';
// import { View } from 'react-native';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Dimensions,
    FlatList,
    TextInput,
    Image,
    TouchableHighlight,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import { fetchProductData } from '../service/apiProduct';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconZocial from 'react-native-vector-icons/Zocial';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native-gesture-handler';


function ProductDetail({ route, navigation }) {

    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [notifyCount, setNotification] = useState(7);

    useEffect(() => {
        fetchProductData(route.params.id).then((result) => {
            // console.log("result",result)
            setData(result)
            setLoading(false)
        })
    }, [])
    return (
        <ScrollView>
            <View style={styles.container}>
                <View >
                    <View style={{ marginBottom: "5%", flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <IconEntypo name="chevron-left" size={27} style={{ color: "#ff6969", right: 70, marginBottom: 60 }} />
                        <View style={{ flexDirection: "column" }}>
                            <Text>{data.description}</Text>
                            <View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: "5%" }}>
                                <Text style={{ fontWeight: "800", fontSize: 15, color: "#696969" }}>{data.Price}</Text>
                                <View style={styles.itemstar}>
                                    <Text style={styles.itemstartext}>{data.rating}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{justifyContent:"flex-end", marginLeft: "10%"}}>
                            <View style={styles.badge}>
                                <Text style={styles.badgetext}>{notifyCount}</Text>
                            </View>
                        <IconIonicons
                         onPress={()=>navigation.navigate('Cart')}
                        name="ios-cart-outline" size={27} style={{ color: "#696969", left: 70, marginBottom: 60 }} />
                        </View>
                    </View>
                    <View style={{justifyContent: "center", alignItems: "center"}}>
                    <View style={styles.itemContainer}>
                        <Image style={styles.item} source={{ uri: data.image }} />
                    </View>
                    </View>
                </View>
                <View style={{ flexDirection: "row", marginBottom: "5%" }}>
                    <View style={styles.menucontainer}>
                    <Text style={{ padding: 13, color: "#ff6969", fontSize: 16 }}>Product</Text>
                    </View>
                    <Text style={{ padding: 13, color: "#696969", fontSize: 16 }}>Details</Text>
                    <Text style={{ padding: 13, color: "#696969", fontSize: 16 }}>Reviews</Text>
                </View>

                <View>
                    <Text style={{ color: "#696969", marginLeft: "3%", marginTop: "5%" }}>SELECT COLOR</Text>
                    <View style={{ flexDirection: "row", marginTop: "5%" }}>
                        <View style={{ height: 30, width: 30, backgroundColor: "pink", borderRadius: 15, marginLeft: "8%" }}></View>
                        <View style={{ height: 30, width: 30, backgroundColor: "white", borderRadius: 15, marginLeft: "8%" }}></View>
                        {/* <View style={{height: 30, width: 30, backgroundColor: "grey", borderRadius: 15, marginLeft: "8%"}}></View> */}
                        <View style={{ height: 30, width: 30, backgroundColor: "maroon", borderRadius: 15, marginLeft: "8%" }}></View>
                        <View style={{ height: 30, width: 30, backgroundColor: "lightgreen", borderRadius: 15, marginLeft: "8%" }}></View>
                        <View style={{ height: 30, width: 30, backgroundColor: "#ff6969", borderRadius: 15, marginLeft: "8%" }}></View>
                        <View style={{ height: 30, width: 30, backgroundColor: "lightblue", borderRadius: 15, marginLeft: "8%" }}></View>
                    </View>
                    <Text style={{ color: "#696969", marginLeft: "3%", marginTop: "10%" }}>SELECT SIZE (US)</Text>
                    <View style={{ flexDirection: "row", marginTop: "5%" }}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}

                        >
                            <TouchableOpacity>
                                <View style={styles.sizecontainer}>
                                    <Text>4.5</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={styles.sizecontainer}>
                                    <Text>5.0</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={styles.sizecontainer}>
                                    <Text>5.5</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={styles.sizecontainer}>
                                    <Text>6.0</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={styles.sizecontainer}>
                                    <Text>6.5</Text>
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: "13%" }}>
                        <TouchableOpacity
                            style={styles.sharebtn}
                        >
                            <View>
                                <Text style={{ color: "#696969", fontWeight: "bold" }}>SHARE THIS</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.addbtn}
                            onPress={() => navigation.navigate('Cart')}
                        >
                            <View>
                                <Text style={{ color: "white", fontWeight: "bold" }}>ADD TO CART</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <Text style={{marginBottom: "30%"}}>SHARE THIS</Text> */}
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        marginTop: "5%"
    },
    itemContainer: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 130,
        height: 170,
        backgroundColor: "#fff",
        margin: 15,
        marginTop: "2%",
        borderRadius: 15,
        marginLeft: 30
    },
    item: {
        position: 'relative',
        width: 120,
        height: 140,
    },

    itemstar: {
        height: 13,
        width: "25%",
        backgroundColor: "#ff6969",
        // marginTop: "3%",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    },

    itemstartext: {
        color: "white",
        fontWeight: "900",
        fontSize: 10,

    },
    sizecontainer: {
        height: 40,
        width: 70,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: "center",
        margin: 13,
        borderRadius: 15
    },
    sharebtn: {
        height: 50,
        width: "45%",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25
    },
    addbtn: {
        height: 50,
        width: "45%",
        backgroundColor: "#ff6969",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25
    },
    menucontainer: {
        justifyContent:"center", 
        alignItems:"center", 
        height: 40,
        width: 100, 
        backgroundColor: "white", 
        borderRadius: 20
    },

    badge: {
        position: 'absolute',
        right: -80,
        top: 15,
        backgroundColor: '#ff6969',
        borderRadius: 8,
        width: 17,
        height: 17,
        justifyContent: 'center',
        alignItems: 'center',
    },

    badgetext: {
        color: 'white',
        fontSize: 10,
        fontWeight: '900',
    }
})

export default ProductDetail;