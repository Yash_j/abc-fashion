import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  ScrollView,
  useWindowDimensions
} from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconEntypo from 'react-native-vector-icons/Entypo';
import { fetchProductData } from '../service/apiProduct';


// const data = [
//   { id: '1', value: 'A', img: require('../assets/0001_fashion_image.jpg'), description: "V Neck Shirt", Price: '$24.99', rating: 4.9 },
// ]

function Cart({ route, navigation }) {
  // const [isLoading, setLoading] = useState(true);
  // const [data, setData] = useState([]);

  // useEffect(() => {

  //   if (typeof (route.params.id) == "undefined") {
  //     console.log(">>>>>>>>>>>>>route.params.id id not define>>>>>>>>")
  //   }
  //   fetchProductData(route.params.id).then((result) => {
  //     // console.log(">>>>>>>",route.params.id)
  //     // console.log("result",result)
  //     console.log(">>>>>>>",result)
  //     setData(result)
  //     setLoading(false)
  //   })
  // }, [])
  const [count, setCount] = useState(1);
  const [notifyCount, setNotification] = useState(1);

  const window = useWindowDimensions();
  // console.log(">>>>window.height", window.height)
  // console.log(">>>>window.width", window.width)

  return (
    <View style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}

      >
        <View>
          <View style={{ flexDirection: "row", marginLeft: 310, marginTop: "8%" }}>
            <View style={{ right: 110 }}>
              <IconEntypo
                onPress={() => navigation.goBack()}
                name="chevron-left" size={30} style={{ right: 200, color: "#ff6969" }} />
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
              <View style={styles.chatbadge}>
                <Text style={styles.badgetext} >{notifyCount}</Text>
              </View>
              <IconIonicons onPress={() => alert('Message')} name="md-chatbubble-outline" size={20} style={{ color: "#808080", right: 40 }} />
              <View style={styles.badge}>
                <Text style={styles.badgetext}>{notifyCount}</Text>
              </View>
              <IconFontAwesome onPress={() => alert('Notification')} name="bell-o" size={20} style={{ color: "#808080", right: 20 }} />
            </View>
          </View>
          <Text style={styles.text}>Cart</Text>
          <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            {/* <Text>Text</Text> */}
            <Image style={styles.cartimage} source={require('../assets/0001_fashion_image.jpg')} />
            <View style={{ flexDirection: "column" }}>
              <Text style={styles.textcolor}>V Neck Grey Shirt</Text>
              {/* <Text style={styles.textcolor}></Text> */}
              <Text style={{ color: "#ff6969", top: 15 }}>$24.99</Text>
              <View style={{ right: 25, flexDirection: "row", top: 20, justifyContent: "space-evenly" }}>
                <TouchableOpacity
                  onPress={() => {
                    console.log(">>>>>", count)
                    if (count > 0) {
                      setCount(count - 1)
                    }
                  }
                  }
                ><View style={styles.countbutton} >
                    <Text>-</Text>
                  </View></TouchableOpacity>
                <Text>{count}</Text>
                <TouchableOpacity
                  onPress={() => setCount(count + 1)}
                ><View style={styles.countbutton}>
                    <Text>+</Text>
                  </View></TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        {/* <Text>Hello</Text> */}

        {(window.width > window.height) ?

          <View style={{ flexDirection: "row" , marginTop: "5%"}}>
            <View style={{ marginRight: "10%" }}>
              <Text style={{ fontSize: 13, color: "#808080" }}>TOTAL</Text>
              <Text style={{ fontWeight: "900", fontSize: 20, color: "#696969" }} >$24.99</Text>
              <Text style={{ fontSize: 12, color: "#696969", fontWeight: "600" }}>FREE DOMESTIC SHIPPING</Text>
            </View>
            <TouchableOpacity
              style={styles.checkbutton}
              onPress={() => navigation.navigate('OrderPlaced')}
            >
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.checkouttext}>CHECKOUT</Text>
                <IconAntDesign style={{ color: "white", left: 20 }} name="rightcircle" size={32} />
              </View>
            </TouchableOpacity>
          </View>

          :

          <View style={{ marginTop: "95%", flexDirection: "row", marginRight: "5%" }}>
            <View style={{ marginRight: "10%" }}>
              <Text style={{ fontSize: 13, color: "#808080" }}>TOTAL</Text>
              <Text style={{ fontWeight: "900", fontSize: 20, color: "#696969" }} >$24.99</Text>
              <Text style={{ fontSize: 12, color: "#696969", fontWeight: "600" }}>FREE DOMESTIC SHIPPING</Text>
            </View>
            <TouchableOpacity
              style={styles.checkbutton}
              onPress={() => navigation.navigate('OrderPlaced')}
            >
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.checkouttext}>CHECKOUT</Text>
                <IconAntDesign style={{ color: "white", left: 20 }} name="rightcircle" size={32} />
              </View>
            </TouchableOpacity>
          </View>
        }

      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  text: {
    fontWeight: "900",
    fontSize: 30,
    color: "#696969",
    marginRight: 200,
    marginBottom: 30
  },
  badge: {
    position: 'absolute',
    right: 10,
    top: 9,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgetext: {
    color: 'white',
    fontSize: 10,
    fontWeight: '900',
  },
  checkbutton: {
    width: "140%",
    backgroundColor: "#ff6969",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    left: 20
    //  marginTop: 40,
    //    marginBottom: 10
  },

  countbutton: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: "#BABABA",
    justifyContent: "center",
    alignItems: "center"
  },
  cartimage: {
    height: 110,
    width: 110,
    borderRadius: 55
  },
  checkouttext: {
    color: "white",
    fontWeight: "900",
    marginTop: "5%",
    // marginLeft: "5%"
  },
  textcolor: {
    color: "#56546B"
  },

  chatbadge: {
    position: 'absolute',
    right: 50,
    top: 9,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  }

});


export default Cart;