import React, {useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Button,
    Alert,
    FlatList,
    Image,
    ScrollView,
    Dimensions,
    // Alert
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import IconMaterialIcons from 'react-native-vector-icons/FontAwesome';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconEntypo from 'react-native-vector-icons/Entypo';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconPayment from 'react-native-vector-icons/MaterialIcons';
import IconFontisto from 'react-native-vector-icons/Fontisto';

import Login from './Login';
// const MoreItems = [
//     { id: 1,img: require('../assets/ShippingLogo.png'), title: "Shipping Address", logo: require('../assets/arrowlogo.png'), marginvalue: "42%"},
//     { id: 2,img: require('../assets/paymentLogo.png') ,title: "Payment Method", logo: require('../assets/arrowlogo.png'), marginvalue: "43%"},
//     { id: 3,img: require('../assets/currency.png') ,title: "Currency", logo: require('../assets/arrowlogo.png'), marginvalue: "60%"},
//     { id: 4,img: require('../assets/LanguageLogo.png') ,title: "Language", logo: require('../assets/arrowlogo.png'), marginvalue: "58%"}
// ]

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


function MoreScreen({ navigation }) {

    const showAlert = ({})=>{

        Alert.alert(  
            'Logout',  
            'Are you sure?',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Logout', onPress: () => navigation.navigate('Login')},  
            ]  
        );  
    }

    const[notifyCount, setNotification] = useState(1);

    return (
        <ScrollView>
            <View style={styles.container}>
                <View>
                    <View style={{ flexDirection: "row", marginLeft: 280, marginBottom: 10 }}>
                        <View style={styles.chatbadge}>
                            <Text style={styles.badgetext}>{notifyCount}</Text>
                        </View>
                        <IconIonicons onPress={()=> alert('Message')} name="md-chatbubble-outline" size={20} style={{ color: "#696969" }} />
                        <View style={styles.badge}>
                            <Text style={styles.badgetext}>{notifyCount}</Text>
                        </View>
                        <IconMaterialIcons onPress={()=> alert('Notification')} style={{ marginLeft: 20, color: "#696969" }} name="bell-o" size={20} />
                    </View>
                    <Text style={styles.text}>More</Text>
                </View>
                <View style={styles.menucontainer}>
                    <TouchableOpacity
                    onPress={()=>alert("Shipping Address")}
                    ><View style={styles.menufield}>
                        {/* <Text>Hello</Text> */}
                        <IconMaterialCommunityIcons name="flag-triangle" size={23} style={{ color: "#696969", right: 10 }} />
                        <Text style={styles.menufieldtext}>Shipping Address</Text>
                        <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969" }} size={20} />
                    </View></TouchableOpacity>

                    <TouchableOpacity
                    onPress={()=>alert("Payment Method")}
                    >
                        <View style={styles.menufield}>
                            {/* <Text>Hello</Text> */}
                            <IconPayment name="payment" size={23} style={{ color: "#696969", right: 10 }} />
                            <Text style={styles.menufieldtext}>Payment Method</Text>
                            <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969" }} size={20} />
                        </View></TouchableOpacity>

                    <TouchableOpacity
                    onPress={()=>alert("Currency")}
                    
                    >
                        <View style={styles.menufield}>
                            {/* <Text>Hello</Text> */}
                            <IconMaterialCommunityIcons name="currency-usd" size={23} style={{ color: "#696969", right: 20 }} />
                            <Text style={{ fontSize: 17, color: "#696969", right: 70 }}>Currency</Text>
                            <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", left: 10 }} size={20} />
                        </View></TouchableOpacity>

                    <TouchableOpacity
                    onPress={()=>alert("Language")}
                    >
                        <View style={styles.menufield}>
                            {/* <Text>Hello</Text> */}
                            <IconFontisto name="language" size={23} style={{ color: "#696969", right: 15 }} />
                            <Text style={{ fontSize: 17, color: "#696969", right: 70 }}>Language</Text>
                            <IconEntypo name="chevron-with-circle-right" style={{ color: "#696969", left: 10 }} size={20} />
                        </View></TouchableOpacity>

                </View>
                {/* <FlatList
                data={MoreItems}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => (
                    <View style={styles.listItem} >
                        <Image style={{height:20,width:20}} source={item.img}/>
                        <Text style={{marginLeft: 20}}>{item.title}</Text>
                        <IconEntypo name="chevron-with-circle-right" style={{marginLeft: item.marginvalue}} size={23}/>
                    </View>
                )}
            /> */}
                <TouchableOpacity
                    // onPress={() => navigation.navigate('Login')}
                    onPress={() => showAlert("hi")}
                >
                    <Text style={styles.logintext}>LOG OUT</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    text: {
        fontWeight: "900",
        fontSize: 30,
        color: "#696969",
        marginRight: 200,
        marginBottom: 30
    },
    container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        marginTop: 30,
        marginBottom: 70

    },
    loginInput: {
        height: 200,
        width: "80%",
        backgroundColor: "#ffffff",
        borderRadius: 13,
    },
    listItem: {
        // flex:1,
        width: 330,
        backgroundColor: "white",
        // borderWidth: 1,
        padding: 17,
        borderRadius: 10,
        borderBottomWidth: 0.3,
        flexDirection: "row"
    },
    badge: {
        position: 'absolute',
        right: -10,
        top: 9,
        backgroundColor: '#ff6969',
        borderRadius: 8,
        width: 17,
        height: 17,
        justifyContent: 'center',
        alignItems: 'center',
    },

    badgetext: {
        color: 'white',
        fontSize: 10,
        fontWeight: '900',
    },
    logintext: {
        marginTop: "40%",
        color: "#ff6969",
        fontWeight: "bold"
    },
    menucontainer: {
        height: 200,
        width: "88%",
        borderRadius: 13,
        backgroundColor: "white",
        justifyContent: "space-around"
    },

    menufield: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    },
    menufieldtext: {
        fontSize: 17,
        color: "#696969",
        right: 40
    },

    chatbadge: {
        position: 'absolute',
        right: 30,
        top: 9,
        backgroundColor: '#ff6969',
        borderRadius: 8,
        width: 17,
        height: 17,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default MoreScreen;