import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  Dimensions,
  TextInput,
  Image,
  ScrollView
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconEntypo from 'react-native-vector-icons/Entypo'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'

const options = [
  { id: 1, title: "Appeal", img: require('../assets/ApparelLogo.png'), name: 'ios-shirt'},
  { id: 2, title: "Beauty", img: require('../assets/BeautyLogo.png'), name: 'lipstick' },
  { id: 3, title: "Shoes", img: require('../assets/ShoesLogo.png'), name: 'shoe-heel' },
  { id: 4, title: "See All", img: require('../assets/SeeAll.png'), name: 'chevron-with-circle-right'}
]

const data = [
  { id: '1', value: 'A', img: require('../assets/0001_fashion_image.jpg'), description: "V Neck Shirt" },
  { id: '2', value: 'B', img: require('../assets/0002_fashion_image.jpg'), description: "V Neck Shirt" },
  { id: '3', value: 'C', img: require('../assets/0003_fashion_image.jpg'), description: "V Neck Shirt" },
];

const imgdata = [
  { id: '1', img: require('../assets/Latest1.png') },
  { id: '2', img: require('../assets/Latest1.png') }
]

// const numColumns = 3;
// const size = Dimensions.get('window').width / numColumns;

function HomeScreen({ navigation }) {

  const[notifyCount, setNotification] = useState(1);

  return (
    <ScrollView>
    <View style={styles.container}>
      {/* <Text>Home Page!</Text> */}
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: "row", marginLeft: 280, marginBottom: 10 }}>
          <View style={styles.chatbadge}>
            <Text style={styles.badgetext}>{notifyCount}</Text>
          </View>
          <IconIonicons onPress={()=> alert('Message')} name="md-chatbubble-outline" size={20} style={{color: "#696969"}}/>
          <View style={styles.badge}>
            <Text style={styles.badgetext}>{notifyCount}</Text>
          </View>
          <IconFontAwesome onPress={()=> alert('Notification')} style={{ marginLeft: 20, color: "#696969" }} name="bell-o" size={20}  />
        </View>
        <Text style={styles.text}>Categories</Text>
        {/* <TouchableOpacity
        onPress={()=> alert('Catagories')}
        > */}
        <FlatList
          horizontal={true}
          data={options}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View style={{ padding: 20 }}>
              <TouchableOpacity
                onPress={() => alert(item.title)}
              >
                <View>
                  {/* <Text>{item.img}</Text>  */}
                  <Image style={{height: 50, width: 50}} source={item.img}/>
                  {/* <View style={styles.categorylogo}>
                    {item.id==1 ? <IconIonicons name={item.name} style={{color: "white",opacity: .7}} size={35}/>: null}
                    {item.id==2 ? <IconMaterialCommunityIcons name={item.name} style={{color: "white",opacity: .7}} size={35}/>: null}
                    {item.id==3 ? <IconMaterialCommunityIcons name={item.name} style={{color: "white",opacity: .7}} size={35}/>: null}
                  </View> */}
                  <Text>{item.title}</Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        />
        {/* <Text>Apparel</Text> */}
        {/* </TouchableOpacity> */}
      </View>
      <View style={{ flex: 2 }}>
        <Text style={styles.textlatest}>Latest</Text>
        {/* <FlatList
          horizontal={true}
          data={imgdata}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <View>
              <Image style={{ marginLeft: "7%" }} source={item.img} />
            </View>
          )}
        /> */}
        <TouchableOpacity
          onPress={()=> navigation.navigate('Search')}
        >
        <Image style={{marginLeft: "7%"}} source={require('../assets/Latest1.png')}/>
        </TouchableOpacity>
        <FlatList
          horizontal={true}
          data={data}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <View style={styles.itemContainer}>
              <Image style={styles.item} source={item.img} />
              <Text >{item.description}</Text>
              
            </View>
          )}
          keyExtractor={item => item.id}
        />

      </View>
    </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 30,
    // marginBottom: "130%"
    flexDirection: "column",
    marginTop: "7%"
  },
  badge: {
    position: 'absolute',
    right: 10,
    top: 10,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },

  badgetext: {
    color: 'white',
    fontSize: 10,
    fontWeight: '900',
  },
  text: {
    fontWeight: "900",
    fontSize: 30,
    color: "#696969",
    // marginRight: 200,
    // marginBottom: 30
  },
  textlatest: {
    fontWeight: "900",
    fontSize: 30,
    color: "#696969",
    marginLeft: "6%",
    marginBottom: "5%"

  },
  itemContainer: {
    position:'relative',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    width: 120,
    height: 160,
    // borderRadius: 10,
    backgroundColor: "#fff",
    margin: 10,
    marginTop: "25%",
    borderRadius: 15
  },
  item: {
    position: 'relative',
    width: 90,
    height:110,
    // justifyContent: "flex-start",
    // alignItems: "flex-start"
    // backgroundColor: 'lightblue',
  },

  categorylogo: {
    display: "flex",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "orange", 
    borderRadius: 35, 
    height: 70, 
    width: 70,


  },
   
  chatbadge: {
    position: 'absolute',
    right: 50,
    top: 10,
    backgroundColor: '#ff6969',
    borderRadius: 8,
    width: 17,
    height: 17,
    justifyContent: 'center',
    alignItems: 'center',
  },

});

export default HomeScreen;